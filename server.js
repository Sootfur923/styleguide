const express = require('express');
const path = require('path');
const app = express();
const port = 3000;
const hbs = require('express-handlebars');

// app.use('/source/', express.static(__dirname));
app.use(express.static(__dirname));

app.engine('hbs', hbs({
  extname : '.hbs',
  layoutsDir : __dirname + '/source/tpl/layouts/',
  partialsDir : __dirname + '/source/tpl/partials/'
}));

app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, 'source/tpl'));

app.get('/', (request, response) => {
  response.render('index', {
    title: 'Vseinstrumenti styleguide',
    content: function () {
      return 'main/home';
    }
  });
})

app.listen(port, (err) => {
  if (err) {
    return console.log(`something bad happened ${err}`);
  }

  console.log(`server is listening on ${port}`);
})