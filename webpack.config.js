const path = require('path');
const extractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
  entry : {
  	main : './source/js/app.js'
  },

  output : {
    path : path.resolve(__dirname, 'source'),
    filename : 'bundle.js',
    publicPath : '/source/'
  },

  module : {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      use: [{
        loader: 'babel-loader',
        options: {
          presets: ['es2015']
        }
      }]
    }, {
      test: /.scss$/,
      use: extractTextPlugin.extract({
        fallback: 'style-loader',
        use: ["css-loader", "sass-loader"]
      })
    }]
  },

  plugins : [
    new extractTextPlugin({
      filename : 'bundle.css'
    })
  ]
};